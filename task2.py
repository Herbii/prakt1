import sys
import random

def generate_sorted_array(size):
    array = [random.randint(1, 100) for _ in range(size)]
    array.sort()
    return array

def binary_search(array, target):
    left = 0
    right = len(array) - 1
    while left <= right:
        mid = (left + right) // 2
        if array[mid] == target:
            return mid
        elif array[mid] < target:
            left = mid + 1
        else:
            right = mid - 1
    return -1

def main():
    if len(sys.argv) != 2:
        print("Usage: python task2.py <target>")
        return

    try:
        target = int(sys.argv[1])
    except ValueError:
        print("Error: Target must be an integer")
        return

    array = generate_sorted_array(100)
    print("Generated array:", array)

    index = binary_search(array, target)
    if index != -1:
        print(f"Цифра {target} найдена по индексу {index} в массиве.")
    else:
        print(f"Цифра {target} не найдена в массиве.")

if __name__ == "__main__":
    main()
